import HomeScreen from "./Screens/Home/HomeScreen";
import LoginScreen from "./Screens/Login/LoginScreen";
import SettingScreen from "./Screens/Settings/SettingScreen";
import SplashScreen from "./Screens/Splash/SplashScreen";
import SplashScreen2 from "./Screens/Splash/SplashScreen2";
import SignupScreen from "./Screens/Login/SignupScreen";
import PrivacyPolicyScreen from "./Screens/Conditions/PrivacyPolicy";
import TermsConditionScreen from "./Screens/Conditions/TermsCondition";


const Config = {
    navigation: {
      
        Splash: {
            screen:SplashScreen,
            navigationOptions: {
                header: null
            }
        },
        
        Splash2: {
            screen:SplashScreen2,
            navigationOptions: {
                header: null
            }
        },
        Login: {
            screen:LoginScreen,
            navigationOptions: {
                header: null
            }
        },
        Home: {
            screen:HomeScreen,
            navigationOptions: {
                header: null
            }
        },
      
        Setting: {
            screen:SettingScreen,
            navigationOptions: {
                header: null
            }
        },

      
        Signup: {
            screen:SignupScreen,
            navigationOptions: {
                header: null
            }
        },
        Privacy: {
            screen:PrivacyPolicyScreen,
            navigationOptions: {
                header: null
            }
        },
        Terms: {
            screen:TermsConditionScreen,
            navigationOptions: {
                header: null
            }
        },
      
        
        
        
        
        


    }
}
export default Config;