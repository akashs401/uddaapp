




//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow

import React, { Component } from 'react';
import { StyleSheet, TouchableHighlight, Image,Alert } from 'react-native';
import { Container, Content, Item, Input, Label, View, Button, Text } from 'native-base';
import bootstrap from 'tcomb-form-native/lib/stylesheets/bootstrap.js';
import backgroundImage from '../../../Assets/logo.jpg';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
var t = require('tcomb-form-native');
var t = require('tcomb-validation');
var validate = t.validate;

validate(1, t.String).isValid();   // => false
validate('a', t.String).isValid();

var result = validate(1, t.String);
result.isValid();             // => false
result.firstError().message;  // => 'Invalid value 1 supplied to String'




var Form = t.form.Form;
var options = {
  auto: 'placeholders',

  fields: {
    PASSWORD: {
      PASSWORD: true,
      secureTextEntry: true,
    },
    CONFIRMPASSWORD: {
      CONFIRMPASSWORD: true,
      secureTextEntry: true,
    },

    EMAILADDRESS: {
      // you can use strings or JSX
      error: 'Insert a valid email'
    },
    FIRSTNAME: {
      // you can use strings or JSX
      error: 'Fill the Firstname field'
    },
    LASTNAME: {
      // you can use strings or JSX
      error: 'Fill the Lastname field'
    }

  },



  stylesheet: bootstrap


};

options.stylesheet.textbox.normal = {
  borderBottomColor: 'black',
  borderBottomWidth: 1,
};
const Email = t.refinement(t.String, email => {
  const reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/; //or any other regexp
  return reg.test(email);
});

// here we are: define your domain model
var Person = t.struct({
  FIRSTNAME: t.String,              // a required string
  LASTNAME: t.String,
  EMAILADDRESS: Email,
  PASSWORD: t.String,
  CONFIRMPASSWORD: t.String,               // a required number
  // a boolean
});

export default class HomeScreen extends Component {
  state = {
    isButtonDisabled: true,
    
    
  };
  onPress = () => {
    // call getValue() to get the values of the form
    var value = this.refs.form.getValue();
    console.log('value',value);
    if (value) { // if validation fails, value will be null
   //   this.props.navigation.navigate('Login');
      if( value.PASSWORD === value.CONFIRMPASSWORD){
        
      // value here is an instance of Person
      fetch('http://java-api.flowzcluster.tk:8080/betting-platform/uddn/betting/api/v1.0/service/users', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: value.FIRSTNAME+" "+value.LASTNAME,
          userName:value.EMAILADDRESS,
          email:value.EMAILADDRESS,
          password: value.PASSWORD,
          phoneNumber: this.props.navigation.state.params.param,
          address: "street two",
          city: "bbsr",
          country: "India",
          zipCode: "560092",

        }),
      })
      //.then((response) => response.json())
        .then((responseJson) => {
         
          this.props.navigation.navigate('Signup');
        })
        .catch((error) => {
          console.error(error);
        });
    }
    else{
      Alert.alert('Confirm Password ','password did not match with confirm password')
    }
  }
}
  onChange(value) {
    this.setState({value});
  }
  
  


  render() {


    return (

      <Container style={styles.contentStyle}>
        <Content style={{ alignContent: "center" }}>
          <View style={{
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <Image
              style={styles.image}
              resizeMode={"contain"}
              source={backgroundImage}
            />
          </View>
          <View><Text style={{ fontSize: hp('%'), marginTop: hp('2%'), fontFamily: 'Roboto-Regular' }}>Loren ipsum dolor
          sit amet.consectetur adipiscing elit.
          sed do eiusmod tempur incididunt ut labore et dolore magna aliqua.</Text></View>

          <View style={styles.container}>
            {/* display */}
            <Form
              ref="form"
              type={Person}
              options={options}
              value={this.state.value}
              onChange={this.onChange.bind(this)}

            />
            <Button onPress={this.onPress} style={styles.button} >
              <Text style={styles.buttonText} >Sign up</Text>
            </Button>
          </View>
          <View>
            <Text style={{ fontSize: hp('1.5%'), marginTop: hp('.3%'), fontFamily: 'Roboto-Regular' }}>By clicking the Sign Up button.you agree to our <Text onPress={() => this.props.navigation.navigate('Terms')} style={{ fontWeight: 'bold', color: '#E2211C', fontWeight: 'bold', textDecorationLine: 'underline' }}>Terms & Conditions</Text> and <Text onPress={() => this.props.navigation.navigate('Privacy')} style={{ fontWeight: 'bold', color: '#E2211C', fontWeight: 'bold', textDecorationLine: 'underline' }}>Privacy policy.</Text></Text>
          </View>
          {/* <Content style={{ padding:10 }}>
                <View><Text style={{ fontSize:20 }}>Loren ipsum dolor
                sit amet.consectetur adipiscing elit.
                sed do eiusmod tempur incididunt ut labore et dolore magna aliqua.</Text></View>
                    <Form>
                        <Item floatingLabel>
                            <Label>FIRST NAME</Label>
                            <Input />
                        </Item>
                        <Item floatingLabel>
                            <Label>LAST NAME</Label>
                            <Input />
                        </Item>
                        <Item floatingLabel>
                            <Label>EMAIL ADDRESS</Label>
                            <Input
                             keyboardType="email-address"
                             autoCorrect={false}
                             autoCapitalize="none"
                             />
                        </Item>
                        <Item floatingLabel>
                            <Label>PASSWORD</Label>

                            <Input secureTextEntry/>
                        </Item>
                        <Item floatingLabel last>
                            <Label>CONFIRM PASSWORD</Label>
                            <Input secureTextEntry/>
                        </Item>


                        <View style={{  marginTop: 40,  }}>
                            <Button onPress={() => this.props.navigation.navigate('Login')} style={{ height: 70, width: 300, marginLeft: 50, backgroundColor: '#E2211C' }}>
                                <Text style={{ fontSize: 25, marginLeft: 92, fontWeight: "bold" }}>Sign Up</Text>
                            </Button>

                           <Text style={{ fontSize: 16, marginTop:10 }}>By clicking the Sign Up button.you agree to our <Text  onPress="{()=>{}}" style={{ fontWeight: 'bold',color: '#E2211C',fontWeight:'bold',textDecorationLine:'underline' }}>Terms & Conditions</Text> and <Text  onPress="{()=>{}}" style={{ fontWeight: 'bold',color: '#E2211C',fontWeight:'bold',textDecorationLine:'underline' }}>Privacy policy.</Text></Text>
                        </View>
                    </Form>
                </Content> */}
        </Content>
      </Container>
    );
  }
}
var styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: hp('1.5%'),
    padding: hp('-2%'),
    backgroundColor: '#ffffff',
    fontSize: hp('.5%')
  },
  buttonText: {
    fontSize: hp('3%'),
    color: 'white',
    alignSelf: 'center'
  },
  button: {

    height: hp('7%'),
    backgroundColor: '#E2211C',
    borderColor: '#E2211C',
    alignSelf: 'stretch',
    justifyContent: 'center',

  },
  image: {
    height: hp('15%'),
    width: wp('70%'),

    //flex:1



  },
  contentStyle: {
    padding: hp('1.5%')
  }
});
