import React, { Component } from 'react';
import { Platform, StyleSheet, Text, TextInput, View, Image } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Drawer, Title, Content, Form, Item, Input } from 'native-base';
import { DrawerActions } from 'react-navigation';
type Props = {};
export default class SettingScreen extends Component<Props> {
    constructor(props) {
        super(props);
      }

    render() {

        return (

            <Container>
                <Header style={{ backgroundColor: '#E2211C' }}
                    androidStatusBarColor="black">

                    <Left>
                        <Button transparent>
                            <Icon name='menu' style={{ fontSize: 30, color: 'white' }} />
                        </Button>
                    </Left>
                    <Body searchBar>
                        <Item style={styles.searchbar}>
                            <Icon name="ios-search" style={{ fontSize: 20, color: 'white' }} />
                            <TextInput
                                placeholder="Search" placeholderTextColor='white' />
                        </Item>
                    </Body>
                    <Right>
                        <Icon name="mail" style={{ fontSize: 30, color: 'white' }} />
                    </Right>
                </Header>
                <Content>
                    <Text>Splash Screen</Text>
                </Content>
            </Container>



        );
    }

}

const styles = StyleSheet.create({

});