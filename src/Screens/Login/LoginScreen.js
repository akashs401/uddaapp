
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow

import React, { Component } from 'react';
import { StyleSheet, Linking, Image, NativeModules, Alert, TouchableOpacity } from 'react-native';
import { SocialIcon } from 'react-native-elements';
import backgroundImage from '../../../Assets/logo.jpg';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Header, Content, Form, Item, Input, Label, View, Button, Text, CheckBox, Body, ListItem, Icon } from 'native-base';

import { handleFbLogin } from '../../lib/auth';
import firebase from '../../config/firebase';


const { RNTwitterSignIn } = NativeModules

const Constants = {
  //Dev Parse keys
  TWITTER_COMSUMER_KEY: "TU7lcko4xP1P5bZN76wSxQ2Jy",
  TWITTER_CONSUMER_SECRET: "AMXkS3kz8azvAImOjr4JH2BEALH1ckmqZFlNf5bDzSaaHv7JN8"
}
export default class LoginScreen extends Component {
  state = {
    isRememberMeChecked: false,
    isLoggedIn: false,
    userName: "",
    password: ""


  }
  handleLogin =() => {
    handleFbLogin().then((auth)=>{
        this.props.navigation.navigate('Signup');
       })
  }

  


  _twitterSignIn = () => {
    RNTwitterSignIn.init(Constants.TWITTER_COMSUMER_KEY, Constants.TWITTER_CONSUMER_SECRET)
    RNTwitterSignIn.logIn()
      .then(loginData => {
        console.log(loginData)
        const { authToken, authTokenSecret } = loginData
        if (authToken && authTokenSecret) {
          this.setState({
            isLoggedIn: true

          })
          this.props.navigation.navigate('Signup');
        }
      })
      .catch(error => {
        console.log(error)
      }
      )
  }

  handleLogout = () => {
    console.log("logout")
    RNTwitterSignIn.logOut()
    this.setState({
      isLoggedIn: false
    })
  }
  onLogin = () => {
    fetch('http://java-api.flowzcluster.tk:8080/betting-platform/uddn/betting/api/v1.0/service/authentication', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        userName: this.state.userName,
        password: this.state.password

      }),
    })
      //.then((response) => response.json())
      .then((responseJson) => {
        // console.log("response object",responseJson.status)
        //console.log("response object",JSON.parse(responseJson. _bodyText).userName)
        //console.log("state object",this.state.userName)
        if (responseJson.status === 200 && JSON.parse(responseJson._bodyText).userName === this.state.userName) {
          // console.log(this.state.userName)
          this.props.navigation.navigate('Signup');
        }
        else {
          //this.props.navigation.navigate('Login');
          Alert.alert('Login Failed', 'Invalid Username or Password');

        }


      })
      .catch((error) => {
        console.error(error);
      });
  }
  onEmailChange(value) {
    this.setState({
      userName: value
    });
  }
  onPasswordChange(value) {
    this.setState({
      password: value
    });
  }


  render() {
    var _this = this;
    return (


      <Container style={styles.contentStyle}>

        <Content>
          {/* <Text>user{this.state.userName}</Text>
          <Text>pass{this.state.userPassword}</Text> */}
          <View style={{
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <Image
              style={styles.image}
              resizeMode={"contain"}
              source={backgroundImage}
            />
          </View>
          <Form>
            <Item floatingLabel>
              <Label style={{ fontFamily: 'Roboto-Regular' }}>EMAIL ADDRESS</Label>

              <Input
                keyboardType="email-address"
                autoCorrect={false}
                autoCapitalize="none"
                value={this.state.userName}
                onChangeText={
                  (value) => { this.onEmailChange(value) }
                }
              />
            </Item>
            <Item floatingLabel>
              <Label style={{ fontFamily: 'Roboto-Regular' }}>PASSWORD</Label>
              <Input secureTextEntry
                value={this.state.password}
                onChangeText={
                  (value) => { this.onPasswordChange(value) }
                }
              />
            </Item>
            <View style={{ flexDirection: "row", marginTop: hp('1.5%') }}>
              <View style={{ flex: 1 }}>
                <Button transparent danger style={{ justifyContent: 'flex-start' }}>
                  <Text style={{ textDecorationLine: 'underline', color: '#E2211C', fontSize: hp('1.5%'), marginTop: hp('1.5%') }}>Forgot password?</Text>
                </Button>
              </View>

              <View style={{ flex: 1 }}>
                <CheckBox checked={this.state.isRememberMeChecked} onPress={() => this.setState({ isRememberMeChecked: !this.state.isRememberMeChecked })}
                  style={{ justifyContent: 'flex-end', marginTop: hp('2.5%'), height: hp('2.5%'), width: wp('5%'), justifyContent: 'center', }} />
                <Body style={{ marginTop: hp('-4%') }} >
                  <Text style={{ justifyContent: 'flex-end', marginTop: hp('1%'), textDecorationLine: 'underline', color: '#E2211C' }} >Remember me</Text>
                </Body>

              </View>
            </View>
            <View>
              <Button onPress={() => this.onLogin()} style={styles.button}>
                <Text style={styles.buttonText}>LOG IN</Text>
              </Button>
              <View style={{ flexDirection: "row", marginTop: hp('1%'), marginLeft: 2 }}>
                <View style={{ flex: 1 }}>
                  <Button transparent danger style={{ justifyContent: 'flex-start' }} onPress={() => this.props.navigation.navigate('Home',{param:this.props.navigation.state.params.param})} >
                    <Text style={{ textDecorationLine: 'underline', fontSize: hp('1.8%'), fontWeight: "bold" }} >CREATE ACCOUNT</Text>
                  </Button>
                </View>

                <View style={{ flex: 1 }}>
                  <Button transparent danger style={{ justifyContent: 'flex-end' }} onPress={() => this.props.navigation.navigate('Signup')} >
                    <Text style={{ textDecorationLine: 'underline', fontSize: hp('1.8%'), fontWeight: "bold", marginLeft: 35 }}>GUEST USER</Text>
                  </Button>


                </View>
              </View>

              <View>

                <SocialIcon
                  title='Log in with Facebook'
                  button
                  type='facebook'
                  onPress={() => { this.handleLogin() }}
                // onPress={() => Linking.openURL('https://facebook.com')}

                />

                <SocialIcon
                  title='Log in with GooglePlus'
                  button
                  type='google-plus-official'
                  onPress={() => Linking.openURL('https://googleplus.com')}
                />

                <SocialIcon
                  title='Log in with Twitter'
                  button
                  type='twitter'
                  onPress={this._twitterSignIn}
                />



              </View>

            </View>
          </Form>
          <View style={{ flexDirection: "row", flex: 0.5, justifyContent: 'center', marginTop: hp('5%') }}>
            <View style={{ flex: 0.10 }}>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('Splash')} >
                <Icon style={styles.iconCircleO} name='circle-o' type='FontAwesome' />

              </TouchableOpacity>
            </View>
            <View style={{ flex: 0.10 }}>
              <TouchableOpacity style={{}} onPress={() => this.props.navigation.navigate('Splash2')}  >
                <Icon style={styles.iconCircleO} name='circle-o' type='FontAwesome' />

              </TouchableOpacity>
            </View>
            <View style={{ flex: 0.10 }}>
              <TouchableOpacity transparent onPress={() => this.props.navigation.navigate('Login')} >
                <Icon style={styles.iconCircle} name='circle' type='FontAwesome' />

              </TouchableOpacity>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

  image: {
    height: hp('13%'),
    width: wp('72%'),
    // flex:1,
    // justifyContent: 'center',
    // alignItems: 'center',

  },
  contentStyle: {
    padding: hp('1.5%')
  },
  buttonText: {
    fontSize: hp('3%'),
    color: 'white',
    alignSelf: 'center',
    justifyContent: 'center'
  },
  button: {

    height: hp('7%'),
    backgroundColor: '#E2211C',
    borderColor: '#E2211C',
    alignSelf: 'center',
    justifyContent: 'center',
    width: wp('90%')

  },
  iconCircle: {
    color: '#999999',
    fontSize: hp('1.5%')

  },
  iconCircleO: {
    color: '#BCBCBC',
    fontSize: hp('1.5%')


  }



});