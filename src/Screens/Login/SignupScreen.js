

import React, { Component } from 'react';
import { Platform, StyleSheet, TextInput, Image, ScrollView, } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Text, View, Icon, Drawer, Title, Content, Form, Item, Input, TabHeading, Circle } from 'native-base';
import { DrawerActions } from 'react-navigation';
import SwitchToggle from 'react-native-switch-toggle';
import backgroundImage from '../../../Assets/profile.png';
import backgroundImageProfile from '../../../Assets/profile2.jpg';
import { Col, Row, Grid } from "react-native-easy-grid";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';

const sports = [
  {
    label: 'Indian Premier League',
    value: 'football',
  },
  {
    label: 'English Premier League',
    value: 'baseball',
  },
  {
    label: 'Australian Premier League',
    value: 'hockey',
  },
];



export default class SignupScreen extends Component<Props> {
  constructor(props) {
    {
      super(props);

      this.inputRefs = {
        firstTextInput: null,
        favSport0: null,
        favSport1: null,
        lastTextInput: null,
      };

      this.state = {
        numbers: [
          {
            label: '1',
            value: 1,
            color: 'orange',
          },
          {
            label: '2',
            value: 2,
            color: 'green',
          },
        ],
        favSport0: undefined,
        favSport1: undefined,
        favSport2: undefined,
        favSport3: undefined,
        favSport4: 'baseball',
        favNumber: undefined,
      };
    }
  }
  render() {
    const placeholder = {
      label: 'Select Your League...',
      value: null,
      color: 'black',
    };

    return (

      <Container>
        <Header style={{ backgroundColor: '#EEEEEE' }}
          androidStatusBarColor="#EEEEEE">

          <Left>
            <Button transparent onPress={() => this.openDrawer()}>
              <Icon name='menu' style={{ fontSize: hp('6.5%'), color: '#E2211C' }} />
            </Button>
          </Left>
          <Body searchBar>
            <Item style={styles.searchbar}>

              <TextInput
                placeholder="SEARCH" placeholderTextColor='#747474' />
              <Icon name="ios-search" style={{ fontSize: hp('3%'), color: '#ADADAD', marginLeft: 150 }} />
            </Item>
            {/* <Title>Tamaltree</Title> */}
          </Body>
          <Right>
            {/* <Icon name="mail" style={{ fontSize: 30, color: '#E2211C' }} /> */}
            <Image
              style={styles.imageProfile}
              resizeMode={"contain"}
              source={backgroundImageProfile}
            />

          </Right>
        </Header>

        <Content>
          <View style={{ flexDirection: "row", backgroundColor: '#DDDDDD' }} >
            <View style={{ flex: 2 }} >
              <Image
                style={styles.image}
                resizeMode={"contain"}
                source={backgroundImage}
              />
            </View>

            <View style={{ flex: 1 }}>
              <Text style={{ marginLeft: -112, fontSize: 10 }}>ACCOUNT NAME</Text>
            </View>
            <View style={{ flex: 1 }}>
              <Text style={{ marginLeft: -85, fontSize: 10 }}>AVAILABLE BALANCE</Text>
            </View>
            <View style={{ marginLeft: -35, backgroundColor: '#CCCCCC', flex: 1.5 }}>
              <View >
                <Text style={{ fontSize: 14, color: '#E2211C' }}>OPEN</Text>
                <Text style={{ fontSize: 14, color: '#E2211C' }}> PLAYS</Text>
              </View>
              {/* <View style={{ backgroundColor: '#CCCCCC' }}>
                  <Text style={{ fontSize: 25, color: '#E2211C' }}>8</Text>
                </View> */}
            </View>
          </View>
          <View style={styles.picker}>
            {/* and value defined */}
            <RNPickerSelect
              placeholder={placeholder}
              items={sports}
              onValueChange={(value) => {
                this.setState({
                  favSport4: value,
                });
              }}

              value={this.state.favSport4}
              useNativeAndroidPickerStyle={true}
              textInputProps={{ underlineColor: 'yellow' }}

            />
          </View>


          <View style={{ flexDirection: "row", marginTop: hp('.5%') }}>
            <View style={{ flex: .8 }}>
              <Button transparent>
                <Icon style={{ marginLeft: hp('1%') }} name='ios-arrow-back' type='Ionicons' />
                <Text style={{ fontSize: hp('1.5%'), marginRight: hp('2%') }}>PAST RESULTS</Text>
              </Button>
            </View>
            <View style={{ flex: .8, marginRight: hp('-2%') }}>
              <Button bordered dark style={{ marginLeft: hp('-1%'), width: hp('14%') }}>
                <Text style={{ color: 'black', alignSelf: 'center' }}>MATCH 5</Text>
              </Button>
            </View>
            <View style={{ flex: .8 }}>
              <Button transparent style={{ marginLeft: hp('-4%') }}>
                <Text style={{ fontSize: hp('1.5%') }}>UPCOMING GAMES</Text>
                <Icon name='ios-arrow-forward' type='Ionicons' />
              </Button>
            </View>
          </View>
          <Grid style={{padding:hp('1%')}}>
            <Row style={{backgroundColor:'#666666'}}>
              <Col><Text style={{fontSize:hp('1.7%'),color:'white'}}>Match Ups</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%'),color:'white'}}>Money Line</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%'),color:'white'}}>Total</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%'),color:'white'}}>Spread</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%'),color:'white'}}>Custom</Text></Col>
            </Row>
            <Row  style={{backgroundColor:'#DEDEDE'}}>
              <Col><Text style={{fontSize:hp('1.7%')}}>SUN JAN 13</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>-2ND QUARTER</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>00:48:30</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>Game Stats</Text></Col>
            </Row>
            <Row  style={{backgroundColor:'#E2211C'}}>
              <Col><Text style={{fontSize:hp('1.7%'),backgroundColor:'#EFEFEF'}}>BURLEY 1</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>-216</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>O 21.5</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>+5.5</Text></Col>
            </Row>
            <Row  style={{backgroundColor:'#E2211C'}}>
              <Col><Text style={{fontSize:hp('1.7%'),backgroundColor:'#EFEFEF'}}>EVERTON 5</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>+216</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>U 21.5</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>-5.5</Text></Col>
              </Row>
              <Row  style={{backgroundColor:'#DEDEDE'}}>
              <Col><Text style={{fontSize:hp('1.7%')}}>SAT JAN 12</Text></Col>
              {/* <Col><Text style={{fontSize:hp('1.7%')}}>-2ND QUARTER</Text></Col> */}
              <Col><Text style={{fontSize:hp('1.7%')}}>08:30PM EST</Text></Col>
            </Row>
            <Row  style={{backgroundColor:'#E2211C'}}>
              <Col><Text style={{fontSize:hp('1.7%'),backgroundColor:'#EFEFEF'}}>FULHAM 1</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>-400</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>O 12.4</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>+5.5</Text></Col>
            </Row>
            <Row  style={{backgroundColor:'#E2211C'}}>
              <Col><Text style={{fontSize:hp('1.7%'),backgroundColor:'#EFEFEF'}}>WANDERERS 5</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>+400</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>U 12.4</Text></Col>
              <Col><Text style={{fontSize:hp('1.7%')}}>-5.5</Text></Col>
              </Row>
          </Grid>


        </Content>
      </Container>



    );
  }

}


const styles = StyleSheet.create({
  searchbar: {
    backgroundColor: '#FFFFFF',
    borderWidth: 7,
    borderStyle: 'solid',
    borderColor: '#CCCCCC',
    width: 230,
    padding: -6,
    height: 40,
    // borderRadius:5
    color: 'white'
  },
  image: {
    height: hp('5%'),
    width: hp('5%'),
    //borderRadius: 40,
  },
  imageProfile: {
    height: hp('5%'),
    width: wp('20%'),
    marginLeft: hp('5%')
    //borderRadius: 40,
  },
  grid: {
    borderWidth: 2,
    borderColor: '#969595',
    borderStyle: 'solid'
  },
  row: {
    marginTop: 5
  },
  picker: {
    fontSize: hp('2%'),
    // paddingHorizontal: 10,
    //paddingVertical: 8,
    borderWidth: hp('.05%'),
    width: wp('80%'),
    height: hp('5%'),
    marginLeft: hp('5%'),
    // borderColor: 'eggplant',
    borderRadius: 0.5,
    color: 'black',
    marginTop: hp('.5%')
    // paddingRight: 30, // to ensure the text is never behind the icon
  },
  // container: {
  //   flex: 1,
  //   flexDirection: 'row',
  //  // alignItems: 'center',
  //   //justifyContent: 'center',
  // }


});




// import React, { Component } from 'react';
// import { Platform, StyleSheet, Text, TextInput, View, Image } from 'react-native';
// import { Container, Header, Left, Body, Right, Button, Icon, Drawer, Title, Content, Form, Item, Input, TabHeading, Circle } from 'native-base';
// import { DrawerActions } from 'react-navigation';
// import SwitchToggle from 'react-native-switch-toggle';
// import backgroundImage from '../../../Assets/profile.png';

// type Props = {};
// export default class SignupScreen extends Component<Props> {
//     constructor(props) {
//         {
//             super(props);
//         }

//     }


//     render() {


//         return (

//             <Container>
//                 <Header style={{ backgroundColor: '#EEEEEE' }}
//                     androidStatusBarColor="#EEEEEE">

//                     <Left>
//                         <Button transparent onPress={() => this.openDrawer()}>
//                             <Icon name='menu' style={{ fontSize: 50, color: '#E2211C' }} />
//                         </Button>
//                     </Left>
//                     <Body searchBar>
//                         <Item style={styles.searchbar}>

//                             <TextInput
//                                 placeholder="SEARCH" placeholderTextColor='#747474' />
//                             <Icon name="ios-search" style={{ fontSize: 20, color: '#ADADAD', marginLeft: 150 }} />
//                         </Item>
//                         {/* <Title>Tamaltree</Title> */}
//                     </Body>
//                     <Right>
//                         <Icon name="mail" style={{ fontSize: 30, color: '#E2211C' }} />
//                     </Right>
//                 </Header>

//                 <Content style={{padding:10}}>
//                     <View style={styles.container}>
//                         <Image
//                             style={styles.image}
//                             resizeMode={"cover"}
//                             source={backgroundImage}
//                         />

//                     </View>


//                 </Content>
//             </Container>



//         );
//     }

// }


// const styles = StyleSheet.create({
//     searchbar: {
//         backgroundColor: '#FFFFFF',
//         borderWidth: 7,
//         borderStyle: 'solid',
//         borderColor: '#CCCCCC',
//         width: 230,
//         padding: -6,
//         height: 40,
//         // borderRadius:5
//         color: 'white'
//     },
//     image: {
//         height: 40,
//         width: 40,
//         borderRadius: 40,
//       }

// });