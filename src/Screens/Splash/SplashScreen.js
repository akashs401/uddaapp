import React, { Component } from 'react';
import { Platform, StyleSheet, Text, TextInput, View, Image, TouchableOpacity, Alert } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Drawer, Title, Content, Form, Item, Input, TabHeading } from 'native-base';
import { DrawerActions } from 'react-navigation';
import SwitchToggle from 'react-native-switch-toggle';
import backgroundImage from '../../../Assets/logo.jpg';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

type Props = {};
export default class SplashScreen extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            isAcknowledged: false

        };
    }



    onPress1 = () => {
        this.setState({ isAcknowledged: !this.state.isAcknowledged });
    }
    onNextPress = () => {
        if (this.state.isAcknowledged) {
            this.props.navigation.navigate('Splash2')
        }
        else{
            Alert.alert('Age verification','Please Acknowledge Age Verification')
        }

    }


    render() {

        return (

            <Container style={styles.contentStyle}>
                <Content >
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Image
                            style={styles.image}
                            resizeMode={"contain"}
                            source={backgroundImage}

                        />
                    </View>

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 16, fontFamily: 'Roboto-Regular', color: '#333333', marginTop: 10, fontFamily: "Roboto" }}>Bet a friend.Bet a stranger.UDDA cuts
                        the vig in half by pairing you with
                        someone who wants your action.Bet anyone, anytime, on anything as
                        long as our team can verify the wager.</Text>
                    </View>


                    <View style={{ flexDirection: "row", marginTop: hp('8%') }}>
                        <View style={{ flex: 1 }} >
                            <SwitchToggle style={{ justifyContent: 'flex-start' }}
                                type={0}
                                containerStyle={{
                                    marginTop: hp('0%'),
                                    width: hp('10%'),
                                    height: hp('6%'),
                                    borderRadius: 28,
                                    backgroundColor: '#E2211C',
                                    padding: 5,
                                }}
                                circleStyle={{
                                    width: hp('5%'),
                                    height: hp('5%'),
                                    borderRadius: 19,
                                    backgroundColor: '#E2211C', // rgb(102,134,205)
                                }}
                                backgroundColorOn='#E2211C'
                                backgroundColorOff='#E2211C'
                                circleColorOff='white'
                                circleColorOn='#808080'
                                switchOn={this.state.isAcknowledged}
                                onPress={this.onPress1}
                            />
                        </View>
                        <View style={{ flex: 3 }}>
                            <Text style={{ justifyContent: 'flex-end', color: '#333333', fontFamily: 'Roboto-Regular' }}> I hereby acknowledge that i am 21 years of age and am Prohibited from allowing any other person to access or use my gaming account.</Text>
                        </View>
                    </View>

                    <View>
                        <Button onPress={this.onNextPress}  style={styles.button}>
                            <Text style={styles.buttonText}>NEXT</Text>
                        </Button>
                    </View>

                    <View style={{ flexDirection: "row", flex: 0.5, justifyContent: 'center', marginTop: hp('5%') }}>
                        <View style={{ flex: 0.10 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Splash')} >
                                <Icon style={styles.iconCircle} name='circle' type='FontAwesome' />

                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.10 }}>
                            <TouchableOpacity style={{}} onPress={() => {
                                if (this.state.isAcknowledged) {
                                    this.props.navigation.navigate('Splash2')
                                }
                            }} >
                                <Icon style={styles.iconCircleO} name='circle-o' type='FontAwesome' />

                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.10 }}>
                        <TouchableOpacity style={{}} onPress={() => {
                                if (this.state.isAcknowledged) {
                                    this.props.navigation.navigate('Login')
                                }
                            }} >
                                <Icon style={styles.iconCircleO} name='circle-o' type='FontAwesome' />

                            </TouchableOpacity>
                        </View>
                    </View>



                </Content>
            </Container>



        );
    }

}


const styles = StyleSheet.create({
    searchbar: {
        backgroundColor: '#FFFFFF',
        borderWidth: 7,
        borderStyle: 'solid',
        borderColor: '#CCCCCC',
        width: 230,
        padding: -6,
        height: 40,
        // borderRadius:5
        color: 'white'
    },
    image: {
        height: hp('30%'),
        width: wp('80%'),

        //flex:1



    },
    button: {

        height: hp('7%'),
        backgroundColor: '#E2211C',
        borderColor: '#E2211C',
        alignSelf: 'center',
        justifyContent: 'center',
        width: wp('90%'),
        marginTop:hp('2%'),
        
      },
    buttonText: {
        fontSize: hp('3%'),
        color: 'white',
        alignSelf: 'center',
        justifyContent: 'center',
        fontSize: hp('4%')

      },
    contentStyle: {
        padding: hp('4%')
    },
    inputFields: {
        fontSize: 20,
        borderStyle: 'solid',
        borderColor: '#000000',
        borderRadius: 30,
        marginBottom: 10
    },

    iconCircle: {
        color: '#999999',
        fontSize: hp('1.5%')

    },
    iconCircleO: {
        color: '#BCBCBC',
        fontSize: hp('1.5%')


    }



});