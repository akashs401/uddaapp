import React, { Component } from 'react';
import { Platform, StyleSheet, Text, TextInput, View, Image, TouchableOpacity, Alert } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Drawer, Title, Content, Form, Item, Input, Label } from 'native-base';
import { DrawerActions } from 'react-navigation';
import SwitchToggle from 'react-native-switch-toggle';
import backgroundImage from '../../../Assets/logo.jpg';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
type Props = {};
export default class SplashScreen2 extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            isAcknowledged1: false,
            isAcknowledged2: false,
            isAcknowledged3: false,
            phoneNumber:""

        };
    }

    onPress1 = () => {
        this.setState({ isAcknowledged1: !this.state.isAcknowledged1 });
    }
    onNextPress = () => {
        if (this.state.isAcknowledged1 && this.state.isAcknowledged2 && this.state.isAcknowledged3) {
            this.props.navigation.navigate('Login',{param:this.state.phoneNumber})
        }
        else{
            Alert.alert('Terms and Conditions','Please accept the Terms of use,privacy policies and location access.')
        }

    }
    onPress2 = () => {
        this.setState({ isAcknowledged2: !this.state.isAcknowledged2 });
    }

    onPress3 = () => {
        this.setState({ isAcknowledged3: !this.state.isAcknowledged3 });
    }
    onphoneNumberChange = (value) =>{
        this.setState({
            phoneNumber:value
        })


    }


    render() {

        return (

            <Container style={styles.contentStyle}>
                <Content>
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Image
                            style={styles.image}
                            resizeMode={"contain"}
                            source={backgroundImage}
                        />
                    </View>
                    <View style={{ marginTop: hp('1.5%'), marginBottom: hp('1%') }}>
                        <Text style={{ textAlign: 'center', fontSize: hp('3%'), color: '#333333' }}>Before<Text style={{ fontWeight: 'bold' }}> you</Text> Can <Text style={{ fontWeight: 'bold' }}>play</Text></Text>
                    </View>
                    <View>
                        <Text style={{ fontSize: hp('1.5%'), color: '#333333', fontFamily: 'Roboto-Regular' }}>If you are an employee of a sports governing body or
                         member team who is not prohibited from wagering on sports you must complete <Text onPress={() => this.props.navigation.navigate('Privacy')} style={{ fontWeight: 'bold', color: '#E2211C', fontWeight: 'bold', textDecorationLine: 'underline' }}>
                                this form </Text>. For more information read our <Text onPress={() => this.props.navigation.navigate('Terms')} style={{ fontWeight: 'bold', color: '#E2211C', fontWeight: 'bold', textDecorationLine: 'underline' }}>Terms of Use</Text>.</Text>
                    </View>
                    <View style={{ marginTop: hp('.3%') }}>
                        <Item>
                            <Input style={{ fontFamily: 'Roboto-Regular' }}
                              value={this.state.phoneNumber}
                              onChangeText={
                                (value) => { this.onphoneNumberChange(value) }
                              }
                             placeholder="Enter your mobile Number for your OTP " />
                        </Item>
                    </View>


                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1 }} >
                            <SwitchToggle style={{ justifyContent: 'flex-start' }}
                                type={0}
                                // containerStyle={{
                                //     marginTop: 16,
                                //     width: 85,
                                //     height: 48,
                                //     borderRadius: 25,
                                //     backgroundColor: '#E2211C',
                                //     padding: 5,
                                // }}
                                containerStyle={{
                                    marginTop: hp('2%'),
                                    width: hp('10%'),
                                    height: hp('6%'),
                                    borderRadius: 28,
                                    backgroundColor: '#E2211C',
                                    padding: hp('.5%'),
                                }}
                                circleStyle={{
                                    width: hp('5%'),
                                    height: hp('5%'),
                                    borderRadius: 19,
                                    backgroundColor: '#E2211C'// rgb(102,134,205)
                                }}
                                backgroundColorOn='#E2211C'
                                backgroundColorOff='#E2211C'
                                circleColorOff='white'
                                circleColorOn='#808080'
                                switchOn={this.state.isAcknowledged1}
                                onPress={this.onPress1}
                            />
                        </View>
                        <View style={{ flex: 3, marginTop: hp('1.5%') }}>
                            <Text style={{ justifyContent: 'flex-end', fontSize: hp('1.5%'), color: '#333333', fontFamily: 'Roboto-Regular' }}>I accept these <Text onPress={() => this.props.navigation.navigate('Terms')} style={{ fontWeight: 'bold', color: '#E2211C', fontWeight: 'bold', textDecorationLine: 'underline' }}>Terms of use</Text>,<Text onPress={() => this.props.navigation.navigate('Privacy')} style={{ fontWeight: 'bold', color: '#E2211C', fontWeight: 'bold', textDecorationLine: 'underline' }}>Privacy policy</Text>.and agree to recieve marketing communications and offers from UDDA.</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1 }} >
                            <SwitchToggle style={{ justifyContent: 'flex-start' }}
                                type={0}
                                containerStyle={{
                                    marginTop: hp('2%'),
                                    width: hp('10%'),
                                    height: hp('6%'),
                                    borderRadius: 28,
                                    backgroundColor: '#E2211C',
                                    padding: hp('.5%'),
                                }}
                                circleStyle={{
                                    width: hp('5%'),
                                    height: hp('5%'),
                                    borderRadius: 19,
                                    backgroundColor: '#E2211C'

                                }}
                                backgroundColorOn='#E2211C'
                                backgroundColorOff='#E2211C'
                                circleColorOff='white'
                                circleColorOn='#808080'
                                switchOn={this.state.isAcknowledged2}
                                onPress={this.onPress2}
                            />
                        </View>
                        <View style={{ flex: 3, marginTop: hp('2%') }}>
                            <Text style={{ justifyContent: 'flex-end', fontSize: hp('1.5%'), color: '#333333', fontFamily: 'Roboto-Regular' }}>Allow UDDA to access your location while you are using the app.This app requires access to your location when the screen is on and the app is displayed.</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1 }} >
                            <SwitchToggle style={{ justifyContent: 'flex-start' }}
                                type={0}
                                containerStyle={{
                                    marginTop: hp('2%'),
                                    width: hp('10%'),
                                    height: hp('6%'),
                                    borderRadius: 28,
                                    backgroundColor: '#E2211C',
                                    padding: hp('.5%'),
                                }}
                                circleStyle={{
                                    width: hp('5%'),
                                    height: hp('5%'),
                                    borderRadius: 19,
                                    backgroundColor: '#E2211C'
                                    // rgb(102,134,205)
                                }}
                                backgroundColorOn='#E2211C'
                                backgroundColorOff='#E2211C'
                                circleColorOff='white'
                                circleColorOn='#808080'
                                switchOn={this.state.isAcknowledged3}
                                onPress={this.onPress3}
                            />
                        </View>
                        <View style={{ flex: 3, marginTop: hp('1.5%') }}>
                            <Text style={{ justifyContent: 'flex-end', fontSize: hp('1.5%'), color: '#333333', fontFamily: 'Roboto-Regular' }}>I certify that all information provided during account registration is true and accurate and that i am not a casino key employee or casino employee prohibited from wagering in a New Jersey casino or simulcasting facility.</Text>
                        </View>
                    </View>
                    <View>
                        {/* <Button onPress={() => this.props.navigation.navigate('Login')} style={{ height: 50, width: 350, backgroundColor: '#E2211C', marginTop: 40, marginLeft: 23 }}>
                            <Text style={{ fontSize: 30, height: 40, width: 300, marginLeft: 120, fontWeight: "bold", color: 'white' }}>Submit</Text>
                        </Button> */}
                        <Button onPress={this.onNextPress} style={{ height: hp('7%'), width: wp('90%'), backgroundColor: '#E2211C', marginTop: hp('2%') }}>
                            <Text style={{ fontSize: hp('4%'), color: 'white', marginLeft: wp('28%') }}>Submit</Text>
                        </Button>
                    </View>
                    <View style={{ flexDirection: "row", flex: 0.5, justifyContent: 'center', marginTop: hp('5%') }}>
                        <View style={{ flex: 0.10 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Splash')} >
                                <Icon style={styles.iconCircleO} name='circle-o' type='FontAwesome' />

                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.10 }}>
                            <TouchableOpacity style={{}} onPress={() => this.props.navigation.navigate('Splash2')}  >
                                <Icon style={styles.iconCircle} name='circle' type='FontAwesome' />

                            </TouchableOpacity>
                        </View>
                        <View style={{ flex: 0.10 }}>
                            <TouchableOpacity transparent onPress={this.onNextPress}>
                                <Icon style={styles.iconCircleO} name='circle-o' type='FontAwesome' />

                            </TouchableOpacity>
                        </View>
                    </View>

                </Content>
            </Container>



        );
    }

}


const styles = StyleSheet.create({
    image: {
        height: hp('15%'),
        width: wp('70%'),
        // flex:1,
        // justifyContent: 'center',
        // alignItems: 'center',

    },
    contentStyle: {
        padding: hp('4%'),
        //  height:hp('100%')
    },
    iconCircle: {
        color: '#999999',
        fontSize: hp('1.5%')

    },
    iconCircleO: {
        color: '#BCBCBC',
        fontSize: hp('1.5%')


    }





});