import Config from './src/config';
import {name as appName} from './app.json';
import { AppRegistry } from 'react-native';
import { StackNavigator } from 'react-navigation';

export const AppNavigator = StackNavigator(Config.navigation);
AppRegistry.registerComponent(appName, () => AppNavigator);
